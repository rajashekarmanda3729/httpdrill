const http = require('http')
const fs = require('fs')
const uuid = require('uuid')

const server = http.createServer((request, response) => {
    if (request.url === '/html' && request.method === 'GET') {
        fs.readFile('./index.html', 'utf-8', (err, data) => {
            if (err) {
                response.writeHead(404, { "Content-Type": "text/html" })
                response.write(err)
            } else {
                response.writeHead(200, { "Content-Type": "text/html" })
                response.write(data)
            }
            response.end()
        })
    }
    if (request.url === '/json' && request.method === 'GET') {
        fs.readFile('./jsonData.json', 'utf-8', (err, data) => {
            if (err) {
                response.writeHead(404, { "Content-Type": "application/json" })
                response.write(err)

            } else {
                response.writeHead(200, { "Content-Type": "application/json" })
                response.write(data)
            }
            response.end()
        })
    }
    if (request.url === '/uuid' && request.method === 'GET') {
        response.writeHead(200, { 'Content-Type': 'application/json' })
        response.write(JSON.stringify(`{uuid: ${uuid.v4()}}`))
        response.end()
    }
    if (request.url.includes('/status/')) {
        let statusCodeNum = request.url.split('/')
        let codeIs = statusCodeNum[statusCodeNum.length - 1]
        if (statusCodeNum.length === 3 && Object.keys(http.STATUS_CODES).includes(codeIs)) {
            let correctCode = parseInt(codeIs)
            response.writeHead(correctCode, { 'Content-Type': 'application/json' })
            response.write(JSON.stringify(`{${correctCode}}: ${http.STATUS_CODES[correctCode]}`))
        } else {
            response.writeHead(404)
            response.write('Error: Please check code url')
        }
        response.end()
    }
    if (request.url.includes('/delay/')) {
        const delayUrl = request.url.split('/')
        const timeInSeconds = parseInt(delayUrl[delayUrl.length - 1])
        if (Number.isInteger(timeInSeconds)) {
            setTimeout(() => {
                response.writeHead(200, { "Content-Type": "application/json" });
                response.write(JSON.stringify({ "Delay seconds": timeInSeconds }));
                response.end();
            }, timeInSeconds * 1000);
        } else {
            response.writeHead(404)
            response.write('delay is not a number please check')
            response.end()
        }
    }

})
server.listen(4000, (err) => {
    if (err) {
        console.log(`something went wrong on litsening server at 4000`)
    } else {
        console.log(`server litsening at 4000`)
    }
})